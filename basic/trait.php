<?php
trait Hello {
    public function sayHello() { 
        echo 'Hello ';    
    }
}
trait World {    
    public function sayWorld() {     
        echo 'World';  
    }
}
class HelloWorld { 
    use Hello, World;  
    public function sayExclamationMark() {   
        echo '!'; 
    }
}
$o = new HelloWorld();
$o->sayHello();
$o->sayWorld();
$o->sayExclamationMark();

class Base {
    public function sayHello() {
        echo 'Hello ';
    }
}
 
trait SayWorld {
    public function sayHello() {
        parent::sayHello();
        echo 'World!';
    }
}
 
class MyHelloWorld extends Base {
    use SayWorld;
}
 
$o = new MyHelloWorld();
$o->sayHello();
?>